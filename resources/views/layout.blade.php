<!DOCTYPE html>
<html>
<head>
    <title>PayU Testing api</title>

    {!! Html::style('css/app.css') !!}

</head>
<body>
<div class="container">

    <h1 class="text-center">PayU testing api</h1>

    <div class="content">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 well">
                @yield ('content')
            </div>
        </div>


    </div>
</div>
</body>
</html>
