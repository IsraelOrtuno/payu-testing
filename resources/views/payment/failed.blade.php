@extends ('layout')

@section ('content')

    <h3>Payment failed</h3>

    <p>{{ $message }}</p>

    <p>Would you like to try again?</p>

    <a href="{{ URL::route('home') }}" class="btn btn-primary">Try again</a>

@stop