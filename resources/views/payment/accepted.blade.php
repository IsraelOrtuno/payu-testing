@extends ('layout')

@section ('content')

    <h3>Payment accepted!</h3>

    <p>Contratulations! Your payment has been accepted!</p>

    <p>{{ $message }}</p>

    <p>Enjoy your apples now!</p>

@stop