@extends ('layout')

@section ('content')

    <h3>Proceed with payment</h3>

    {!! Form::open(['route' => 'pay', 'method' => 'POST']) !!}

        <div class="form-group {{ ! $errors->has('amount') ?: 'has-error' }}">
            <label for="amount">Amount:</label>
            <div class="input-group">

                {!! Form::text('amount', null, ['placeholder' => 'Amount to pay', 'class' => 'form-control']) !!}

                {{-- Not using number due to browsers compatibility, but would work much better --}}
                {{-- Form::number('amount', null, ['placeholder' => 'Amount to pay', 'class' => 'form-control']) --}}

                <div class="input-group-addon">.00</div>

            </div>
            <p class="help-block ">{{ $errors->first('amount') }}</p>
        </div>

        <button type="submit" class="btn btn-success">Pay now!</button>

    {!! Form::close() !!}

@stop