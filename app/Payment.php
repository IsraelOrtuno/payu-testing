<?php

namespace App;

use SoapVar;
use SoapClient;
use SoapHeader;

class Payment
{
    /**
     * Default amount to charge.
     *
     * @var int
     */
    protected $amount = 1000;

    /**
     * Will carry out the transaction process.
     *
     * @return mixed
     */
    public function performTransaction()
    {
        $transaction = $this->getSetTransactionData();

        // Will execute the request and return the result.
        return $this->getSoapClient()
                    ->setTransaction($transaction);
    }

    /**
     * Will check if a transaction reference exists in PayU.
     *
     * @param $reference
     *
     * @return mixed
     */
    public function confirmTransaction($reference)
    {
        $transaction = $this->getGetTransactionData($reference);

        return $this->getSoapClient()
                    ->getTransaction($transaction);
    }

    /**
     * Provides the Soap Client with headers already set.
     */
    protected function getSoapClient()
    {
        $header = $this->getSoapHeader();

        // Setting up the Soap Client. URL to call and headers.
        $client = new SoapClient(env('PAYU_WDSL_URL'), ["trace" => 1, "exception" => 0]);
        $client->__setSoapHeaders($header);

        return $client;
    }

    /**
     * Will append the PayUReference to the RPP Url.
     *
     * @param $reference
     *
     * @return string
     */
    public function getRppUrl($reference)
    {
        return env('PAYU_RPP_URL') . $reference;
    }

    /**
     * Generates the right SoapHeader object
     *
     * @return SOAPHeader
     */
    protected function getSoapHeader()
    {
        // Instead of using an XML file and replaces we could have used arrays, but there
        // is a big lack of docs for using these native PHP classes for performing
        // such a simple task as generating an XML structure.
        $headerContent = new SoapVar($this->getXMLHeaderContent(), XSD_ANYXML);
        $namespace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

        return new SoapHeader($namespace, 'Security', $headerContent, true);
    }

    /**
     * Provides the XML headers setting the username and password.
     *
     * @return mixed
     */
    protected function getXMLHeaderContent()
    {
        $content = \Storage::get('header.xml');

        return str_replace(
            ['{username}', '{password}'],
            [env('PAYU_USERNAME'), env('PAYU_PASSWORD')],
            $content
        );
    }

    /**
     * Provides an array with the transaction details.
     *
     * @return array
     */
    protected function getSetTransactionData()
    {
        return array_merge($this->getApiData(), [
            'TransactionType'       => 'PAYMENT',

            'AdditionalInformation' => [
                'merchantReference'       => 100285,
                'cancelUrl'               => \URL::route('cancel'),
                'returnUrl'               => \URL::route('return'),
                'supportedPaymentMethods' => 'CREDITCARD'
            ],
            'Basket'                => [
                'description'   => '1KG of Apples',
                'amountInCents' => $this->amount,
                'currencyCode'  => 'ZAR'
            ],
            'Customer'              => [
                'merchantUserId' => '2',
                'email'          => 'io@mail.com',
                'firstName'      => 'Israel',
                'lastName'       => 'Ortuno',
                'mobile'         => '0123456789',
                'regionalId'     => '968',
                'countryCode'    => '34'
            ]
        ]);
    }

    /**
     * Provides the getTransaction request information.
     *
     * @param $reference
     *
     * @return array
     */
    protected function getGetTransactionData($reference)
    {
        return array_merge($this->getApiData(), [
            'AdditionalInformation' => [
                'payUReference' => $reference
            ]
        ]);
    }

    /**
     * Provides the basic API data.
     *
     * @return array
     */
    protected function  getApiData()
    {
        return [
            'Api'     => 'ONE_ZERO',
            'Safekey' => env('PAYU_SAFEKEY')
        ];
    }

    /**
     * Setting the amount. It accepts regular units and will
     * set cents.
     *
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount * 100;
    }

}