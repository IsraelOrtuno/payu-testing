<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Payment;

class PaymentController extends Controller
{

    /**
     * Displaying the payment form.
     */
    public function index()
    {
        return view('payment.index');
    }

    /**
     * @param Payment        $payment
     *
     * @param PaymentRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function pay(Payment $payment, PaymentRequest $request)
    {
        try
        {
            // Setting the amount to charge. It has automatically been validated
            // on the PaymentRequest form validation so we are sure the input
            // variable is available and we do not need to check for it.
            $payment->setAmount(request()->get('amount'));

            // Once set, just perform the transaction request and wait for its
            // result. If "successful", it means everything went ok.
            $result = $payment->performTransaction();
            $result = $result->return;

            if ( ! $result->successful)
            {
                return view('payment.failed')
                    ->withMessage('There was an error while processing your transaccion. Please try again');
            }

            return redirect()->away($payment->getRppUrl($result->payUReference));
        }
        catch (Exception $e)
        {
            return view('payment.failed')
                ->withMessage('Unexpected error.');
        }
    }

    /**
     * @param Payment $payment
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function result(Payment $payment)
    {
        try
        {
            $result = $payment->confirmTransaction(request()->get('PayUReference'));
            $result = $result->return;

            // This will handle the transaction successfulness. If any ot these rules
            // do not pass, we may predict there was an error in the transaction.
            // Will display the failed view and notify something went wrong.
            if ($result->successful !== true || $result->transactionState !== 'SUCCESSFUL' || $result->resultCode !== '00')
            {
                return view('payment.failed')
                    ->withMessage('An error has ocurred while processing your transaction.');
            }

            // If we reach this point, everything has gone OK so we can assume that
            // the transaction was successful and the payment has been accepted.
            // Showing a confirmation message and the transaction reference.
            return view('payment.accepted')
                ->withMessage("Reference: {$result->payUReference}");
        }
        catch (Exception $e)
        {
            return view('payment.failed')
                ->withMessage('Unexpected error.');
        }
    }

    /**
     * Paymen is manually cancelled.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancelled()
    {
        return view('payment.failed')
            ->withMessage('Your payment has been cancelled.');
    }

}