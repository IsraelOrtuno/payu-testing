<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'PaymentController@index']);
Route::post('pay', ['as' => 'pay', 'uses' => 'PaymentController@pay']);
Route::get('result', ['as' => 'return', 'uses' => 'PaymentController@result']);
Route::get('cancelled', ['as' => 'cancel', 'uses' => 'PaymentController@cancelled']);

