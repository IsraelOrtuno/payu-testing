<?php

namespace App\Http\Requests;

class PaymentRequest extends Request
{

    /**
     * Custom form rules, just requiring the amount field.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:1'
        ];
    }

    /**
     * Authorize request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}