# PayU Testing API

## 1. Install dependences.

Run `composer install`.

# 2. Environment variables

Set the environment variables from the `.env.example` or rename that file to just `.env`.

# 3. Set the application key

Run `php artisan key:generate` if using `.env` or create an `APP_KEY` 32 length random string.

## 4. Run the app

Navigate to the home director `/` and check.